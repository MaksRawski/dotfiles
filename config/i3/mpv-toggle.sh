#!/bin/bash
mpv=$(pidof mpv)
if [ "$(cat /proc/$mpv/task/$mpv/status | grep State)" == "State:	T (stopped)" ]; then
	kill -s CONT $(pidof mpv)
	echo "Playing"
else
	kill -s STOP $(pidof mpv)
	echo "Stopping"
fi
