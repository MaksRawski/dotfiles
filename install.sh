cp ~/.config/i3/config ~/.config/i3/config.backup
cp -r config/* ~/.config/
cp .Xresources ~/
xrdb ~/.Xresources
cp .vimrc ~/
cp -r .vim ~/
if ! [ -e ~/.vim/bundle/Vundle.vim ];then
	git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim	
fi
chmod +x ~/.config/ranger/scope.sh
chmod +x ~/.config/i3blocks/*
chmod -x ~/.config/i3blocks/config
if ! [ -e ~/.zshrc ]; then
	sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
	cp .zshrc ~/
fi
sudo cp i3exit /usr/bin/i3exit
sudo chmod +x /usr/bin/i3exit
#cd ~/.vim/bundle/YouCompleteMe/ && python install.py --clang-completer --ts-completer && cd
if command -v pacman > /dev/null; then
	sudo pacman -Syu dmenu htop ranger w3m ttf-hack feh zsh nodejs npm mpd mps-youtube ncmpcpp xss-lock i3blocks lm_sensors upower curl pulseaudio pulsemixer mpv
	if ! command -v yay > /dev/null; then
		git clone https://aur.archlinux.org/yay.git
		cd yay && makepkg -si && cd .. && rm -rf yay
		yay -S cava ttf-iosevka-term --noconfirm
	fi
fi

if command -v apt > /dev/null; then  
	sudo apt install dmenu htop ranger w3m fonts-hack-ttf feh zsh nodejs npm mpd mps-youtube ncmpcpp xss-lock i3blocks lm-sensors upower w3m-img curl wget torbrowser-launcher
fi
